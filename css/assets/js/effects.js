var timer = 0;
function recheck() {
    var window_top = $(this).scrollTop();
    var window_height = $(this).height();
    var view_port_s = window_top;
    var view_port_e = window_top + window_height;

    if ( timer ) {
        clearTimeout( timer );
    }

    $('.fly').each(function(){
        var block = $(this);
        var block_top = block.offset().top;
        var block_height = block.height();
        var block_end = block_top + block_height + 100;

        if ( block_top < view_port_e &&  window_top < block_end) {
            timer = setTimeout(function(){
                block.addClass('show-block');
            },200);
        } /*else {
            timer = setTimeout(function(){
                block.removeClass('show-block');
            },200);
        }*/
    });
}

$(function(){
    $(window).scroll(function(){
        recheck();
    });

    $(window).resize(function(){
        recheck();
    });

    recheck();
});

/*
$(function(){

    var $window = $(window);		//Window object

    var scrollTime = 1.2;			//Scroll time
    var scrollDistance = 170;		//Distance. Use smaller value for shorter scroll and greater value for longer scroll

    $window.on("mousewheel DOMMouseScroll", function(event){

        event.preventDefault();

        var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
        var scrollTop = $window.scrollTop();
        var finalScroll = scrollTop - parseInt(delta*scrollDistance);

        TweenMax.to($window, scrollTime, {
            scrollTo : { y: finalScroll, autoKill:true },
            ease: Power1.easeOut,	//For more easing functions see https://api.greensock.com/js/com/greensock/easing/package-detail.html
            autoKill: true,
            overwrite: 5
        });

    });

});*/