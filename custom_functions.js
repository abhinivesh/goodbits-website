
//Function for splitting the email address and returning the domain and domain name
function get_email_domain(email) {
    var values = email.toLowerCase().split("@");
    var domain = values[1];
    var domain_name = values[1].split(".")[0];
    return [domain, domain_name]
}

//Function for checking whether provided email is official or not
function is_valid_email(email, mail_domains, mail_domain_names) {
    var parse_response = get_email_domain(email);
    if (mail_domains.includes(parse_response[0]) === true) {
        return false;
    }
    else {
        if (mail_domain_names.includes(parse_response[1]) === true) {
            return false;
        }
    }
    return true;
}
